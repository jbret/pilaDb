package main

import (
	"net/http"
	"../pilaDb/src/github.com/gorilla/mux"
)

type httpApi interface {
	push(w http.ResponseWriter, r *http.Request)
	pop(w http.ResponseWriter, r *http.Request)
}

type PilaHttpApi struct {
	pila *Pila
}

func MakePilaHttpApi(pila *Pila) *PilaHttpApi {
	return &PilaHttpApi{
		pila: pila,
	}
}

func (api *PilaHttpApi) push(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	err := api.pila.push(vars["s"])

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Element saved correctly"))
}

func (api *PilaHttpApi) pop(w http.ResponseWriter, r *http.Request) {
	poppedElement, err := api.pila.pop()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(poppedElement))
}
