package main

import (
	"fmt"
	"testing"
)

const pilaSize int = 2

var pilaDb *Pila = MakePila(pilaSize)

func TestReachMaxLimitAndThrowError(t *testing.T) {

	var err error = nil

	for i := 0; i < pilaSize; i++ {
		err = pilaDb.push("Test" + string(i))
	}

	err = pilaDb.push("This should make an error")

	if err == nil {
		t.Error("The limit is not reached and it should be and error")
	}
}

func TestReachMinLimitAndError(t *testing.T) {
	var err error = nil
	var lastElem string = ""

	pilaDb.push("test")
	lastElem, err = pilaDb.pop()
	fmt.Printf(lastElem)
	lastElem, err = pilaDb.pop()
	fmt.Println(lastElem)

	if err == nil {
		t.Error("It should throw an error")
	}
}
