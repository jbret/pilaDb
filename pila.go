package main

import "errors"

type Pila struct {
	pila    []string
	current int
	size    int
}

func MakePila(size int) *Pila {
	pilaDb := &Pila{
		pila: make([]string, size),
		current: -1,
		size: size,
	}

	return pilaDb
}

func (pila *Pila) push(s string) error {

	if pila.isReachedMaxLimit() {
		return errors.New("The pila is Full!")
	}

	pila.current++
	pila.pila[pila.current] = s

	return nil
}

func (pila *Pila) pop() (string, error) {
	var err error = nil

	if pila.isReachedMinLimit() {
		return "", errors.New("The pila is Empty!")
	}
	aux := pila.pila[pila.current]
	pila.pila[pila.current] = ""
	pila.current--

	return aux, err
}

func (pila *Pila) isReachedMinLimit() bool {
	return pila.current < 0
}

func (pila *Pila) isReachedMaxLimit() bool {
	return pila.current >= len(pila.pila) - 1
}
