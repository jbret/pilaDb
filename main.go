package main

import (
	"../pilaDb/src/github.com/gorilla/mux"
	"fmt"
	"net/http"
)

var (
	pilaDb *Pila
	pilaApi *PilaHttpApi
)


func main() {
	var err error
	pilaDb = MakePila(1024)
	pilaApi = MakePilaHttpApi(pilaDb)

	fmt.Println("PilaDB API Rest2")

	r := mux.NewRouter()
	r.HandleFunc("/push/{s}", pilaApi.push)
	r.HandleFunc("/pop", pilaApi.pop)
	err = http.ListenAndServe(":8080", r)
	fmt.Println(err)
}
